Source: libevhtp
Section: libs
Priority: optional
Maintainer: Vincent Bernat <bernat@debian.org>
Standards-Version: 4.1.2
Build-Depends: debhelper-compat (= 13),
               cmake,
               libevent-dev,
               libonig-dev,
               libssl-dev,
               doxygen
Homepage: https://github.com/criticalstack/libevhtp
Vcs-Git: https://salsa.debian.org/debian/libevhtp.git
Vcs-Browser: https://salsa.debian.org/debian/libevhtp

Package: libevhtp0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Libevent based HTTP API
 Libevent's http interface was created as a JIT server, never meant to
 be a full-fledged HTTP service.  This library attempts to improve on
 that with the following features:
 .
  + design as a fully functional HTTP server
  + HTTP parser able to process data with a low memory footprint
  + use of regular expressions for routing
  + out-of-the box HTTPS server
 .
 This package contains the runtime library.

Package: libevhtp-dev
Section: libdevel
Architecture: any
Depends: libevhtp0 (= ${binary:Version}), libonig-dev, ${misc:Depends}
Suggests: libevhtp-doc
Description: Libevent based HTTP API - development files
 Libevent's http interface was created as a JIT server, never meant to
 be a full-fledged HTTP service.  This library attempts to improve on
 that.
 .
 This package contains the development library.

Package: libevhtp-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Libevent based HTTP API - documentation
 Libevent's http interface was created as a JIT server, never meant to
 be a full-fledged HTTP service.  This library attempts to improve on
 that.
 .
 This package contains the documentation and examples.
